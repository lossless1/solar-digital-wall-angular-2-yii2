<?php

namespace app\controllers;

use app\models\Comments;
use Yii;
use yii\base\Component;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class ApiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionComments()
    {
        $params = Yii::$app->request->get();

        if (isset($params['action'])) {
            switch ($params['action']) {
                case 'get-comments-all':
                    $keys = ['action' => true];
                    $data = Comments::getAll();
                    $this->checkRequiredKey($keys, $params);
                    return json_encode(['action' => $params['action'], 'status' => true, 'data' => $data]);
                    break;
                case 'get-comment-by-id': {
                    $keys = ['action' => true, 'id' => true];
                    Yii::$app->logs->saveLog(106, 'Getting comments by id');
                    $this->checkRequiredKey($keys, $params);
                    $data = Comments::getById($params['id']);
                    return json_encode(['action' => $params['action'], 'status' => true, 'data' => $data]);
                }
                case 'add-comment': {
                    $keys = ['action' => true, 'text' => true, 'username' => true];
                    Yii::$app->logs->saveLog(102, 'New comment ' . $params['username']);
                    $this->checkRequiredKey($keys, $params);
                    $data = Comments::addNew($params['text'], $params['username']);
                    return json_encode(['action' => $params['action'], 'status' => true, 'data' => $data]);
                }
                case 'reply-comment': {
                    $keys = ['action' => true, 'id' => true, 'text' => true, 'username' => true];
                    Yii::$app->logs->saveLog(102, 'Reply comment ' . $params['id'] . " " . $params['text'] . " " . $params['username']);
                    $this->checkRequiredKey($keys, $params);
                    $data = Comments::replyById($params['id'],$params['text'], $params['username']);
                    return json_encode(['action' => $params['action'], 'status' => true, 'data' => $data]);
                }
                case 'redact-comment': {
                    $keys = ['action' => true, 'id' => true, 'text' => true];
                    Yii::$app->logs->saveLog(102, 'Redact comment ' . $params['id'] . " " . $params['text']);
                    $this->checkRequiredKey($keys, $params);
                    $data = Comments::redactById($params['id'], $params['text']);
                    return json_encode(['action' => $params['action'], 'status' => true, 'data' => $data]);
                }
                case 'delete-comment': {
                    $keys = ['action' => true, 'id' => true];
                    Yii::$app->logs->saveLog(102, 'Deleting comment ' . $params['id']);
                    $this->checkRequiredKey($keys, $params);
                    $data = Comments::deleteById($params['id']);
                    return json_encode(['action' => $params['action'], 'status' => true, 'data' => $data]);
                }
                case 'delete-comments-all': {
                    $keys = ['action' => true];
                    Yii::$app->logs->saveLog(102, 'Deleting all comments');
                    $this->checkRequiredKey($keys, $params);
                    $data = Comments::deleteAllComments();
                    return json_encode(['action' => $params['action'], 'status' => true, 'data' => $data]);
                }
                default: {
                    $this->setHeader(404);
                    return json_encode(['status' => false, 'message' => 'Method not found']);
                    break;
                }
            }
        } else {
            $this->setHeader(400);
            return json_encode(['status' => false]);
        }
    }


    private function setHeader($status)
    {
        header('Access-Control-Allow-Origin:*');
        Yii::$app->request->headers->add('Access-Control-Allow-Origin', '*');
        Yii::$app->response->setStatusCode($status, $this->_getStatusCodeMessage($status));
    }

    private function _getStatusCodeMessage($status)
    {
        $codes = [
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        ];
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    private function checkRequiredKey($keys = array(), $data = array())
    {
        if (!empty($data)) {
            $out = $this->arrayDiffKeyRecursive($keys, $data);
            if (!empty($out)) {
                $this->setHeader(404);
                die(json_encode($out));
            } else {
                $this->setHeader(200);
            }
        }
    }

    private function arrayDiffKeyRecursive(array $keys, array $data)
    {
        $diff = array_diff_key($keys, $data);
        $intersect = array_intersect_key($keys, $data);

        foreach ($intersect as $k => $v) {
            if (is_array($keys[$k]) && is_array($data[$k])) {
                $d = self::arrayDiffKeyRecursive($keys[$k], $data[$k]);
                if ($d) {
                    $diff[$k] = $d;
                }
            }
        }
        return $diff;
    }
}
