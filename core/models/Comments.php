<?php

namespace app\models;

use Codeception\Lib\Actor\Shared\Comment;
use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "comments".
 *
 * @property int $id_comment
 * @property int $id_parent_comment
 * @property string $user_name
 * @property string $text_comment
 * @property string $created
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent_comment'], 'integer'],
            [['user_name', 'text_comment'], 'required'],
            [['text_comment'], 'string'],
            [['created'], 'safe'],
            [['user_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_comment' => 'Id Comment',
            'id_parent_comment' => 'Id Parent Comment',
            'user_name' => 'User Name',
            'text_comment' => 'Text Comment',
            'created' => 'Created',
        ];
    }

    public static function addNew($text, $username)
    {
        $comment = new self();
        $comment->id_parent_comment = 0;
        $comment->user_name = $username;
        $comment->text_comment = $text;
        if (!$comment->save()) {
            Yii::$app->logs->saveLog(108, 'Error add new comment');
        }
    }

    public static function redactById($id, $text)
    {
        $result = true;
        $data = [];
        try {
            $data = Comments::find()->where(['id_comment' => $id])->one();
            $data->text_comment = $text;
            if(!$data->save()){
                throw new Exception('Error redact comment');
            }
        } catch (Exception $e) {
            Yii::$app->logs->saveLog(107, $e);
        }
        return ['result' => $result, 'data' => $data];
    }

    public static function replyById($id, $text, $user)
    {
        $result = true;
        $data = [];
        try {
            $model = new self;
            $model->id_parent_comment = $id;
            $model->user_name = $user;
            $model->text_comment = $text;
            if(!$model->save()){
                throw new Exception('Error reply comment');
            }
        } catch (Exception $e) {
            Yii::$app->logs->saveLog(107, $e);
        }
        return ['result' => $result, 'data' => $data];
    }

    public static function getAll()
    {
        $result = true;
        $data = [];
        try {
            $data = Comments::find()->asArray()->all();
        } catch (Exception $e) {
            Yii::$app->logs->saveLog(109, 'Error get all comments');
        }
        return ['result' => $result, 'data' => $data];
    }

    public static function getById($id)
    {
        $result = true;
        $data = [];
        try {
            $data = Comments::find()->where(['id_comment' => $id])->asArray()->all();
        } catch (Exception $e) {
            Yii::$app->logs->saveLog(110, 'Error get comments by id');
        }
        return ['result' => $result, 'data' => $data];
    }

    public static function deleteById($id)
    {
        $result = true;
        try {
            $data = Comments::find()->where(['id_comment' => $id])->one();
            $data->delete();
        } catch (Exception $e) {
            Yii::$app->logs->saveLog(111, 'Error delete by id');
        }
        return ['result' => $result];
    }

    public static function deleteAllComments()
    {
        $result = true;
        try {
            Comments::deleteAll();
        } catch (Exception $e) {
            Yii::$app->logs->saveLog(112, 'Error delete all comments');
        }
        return ['result' => $result];
    }
}
