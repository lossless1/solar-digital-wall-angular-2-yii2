<?php

use yii\db\Migration;

class m170427_155228_comments extends Migration
{
    public function up()
    {
        $this->createTable('comments',[
            'id_comment'=>$this->primaryKey(),
            'id_parent_comment'=>$this->integer(11)->notNull()->defaultValue(0),
            'user_name'=>$this->string(255)->notNull(),
            'text_comment'=>$this->text()->notNull(),
            'created'=>$this->timestamp(),
        ]);
        $this->createIndex('id_comment','comments','id_comment');
    }

    public function down()
    {
        $this->dropIndex('id_comment','comments');
        $this->dropTable('comments');
    }
}
