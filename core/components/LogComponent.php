<?php
/**
 * Created by PhpStorm.
 * User: lossless
 * Date: 4/27/17
 * Time: 20:38
 */

namespace app\components;

use Yii;
use yii\base\Component;
use app\models\Logs;
use yii\base\InvalidConfigException;

class LogComponent extends Component
{
    /*
     * 100: Error comment
     * 101: Error name
     * 102: New Comment
     * 103: Redact Comment
     * 104: Delete Comment
     * 105: Getting all comments
     * 106: Getting comment by id
     * 107: Error redact comment
     * 108: Error add new comment
     * 109: Error get all comments
     * 110: Error get comments by id
     * 111: Error delete by id
     * 112: Error delete all comments
     */
    public function saveLog($status, $text)
    {
        $log = new Logs();
        $log->status = $status;
        $log->text_log = $text;
        $log->save();
    }
}