import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {AppRequest} from "./app.request";
import {ICommentsData} from "./models/ICommentsData";

@Injectable()
export class AppService {

    constructor(private appRequest: AppRequest) {
    }

    private source = new Subject<ICommentsData[]>();

    missions = this.source.asObservable();

    getCommentsAll() {
        this.appRequest.getCommentsAll()
            .subscribe((data) => {
                    if (data.result) {
                        this.source.next(data.data);
                        console.log(data);
                    }
                },
                (error) => {
                    console.log(error);
                })
    }

    getCommentById(id) {
        this.appRequest.getCommentById(id)
            .subscribe((data) => {
                    if (data.result) {
                        console.log(data);
                    }
                },
                (error) => {
                    console.log(error);
                })
    }

    addComment(text, username) {
        this.appRequest.addComment(text, username)
            .subscribe((data) => {
                    if (data.result) {
                        console.log(data);
                    }
                },
                (error) => {
                    console.log(error);
                })
    }

    redactComment(id, text) {
        this.appRequest.redactComment(id, text)
            .subscribe((data) => {
                    if (data.result) {
                        console.log(data);
                    }
                },
                (error) => {
                    console.log(error);
                })
    }
    replyComment(id,text,username){
        this.appRequest.replyComment(id, text,username)
            .subscribe((data) => {
                    if (data.result) {
                        console.log(data);
                    }
                },
                (error) => {
                    console.log(error);
                })
    }

    deleteComment(id) {
        this.appRequest.deleteComment(id)
            .subscribe((data) => {
                    if (data.result) {
                        console.log(data);
                    }
                },
                (error) => {
                    console.log(error);
                })
    }

    deleteAllComments() {
        this.appRequest.deleteAllComments()
            .subscribe((data) => {
                    if (data.result) {
                        console.log(data);
                    }
                },
                (error) => {
                    console.log(error);
                })
    }
}
