///<reference path="models/IStatus.ts"/>
import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";
import * as Rx from "rxjs";
import {IComments} from "./models/IComments";
import {CONFIG} from "./app.config";
import {IStatus} from "./models/IStatus";

@Injectable()
export class AppRequest {

    constructor(private http: Http) {

    }

    getCommentsAll(): Rx.Observable<IComments> {
        let query = [
            `action=get-comments-all`,
        ].join("&");
        let fullQuery = `${CONFIG.serverUrl}/comments?${query}`;
        return this.http.request(fullQuery)
            .map(this.extractData)
            .catch(this.handleError);
    }

    getCommentById(id: number): Rx.Observable<IComments> {
        let query = [
            `action=get-comment-by-id`,
            `id=${id}`
        ].join("&");
        let fullQuery = `${CONFIG.serverUrl}/comments?${query}`;
        return this.http.request(fullQuery)
            .map(this.extractData)
            .catch(this.handleError);
    }

    addComment(text, username): Rx.Observable<IStatus> {
        let query = [
            `action=add-comment`,
            `text=${text}`,
            `username=${username}`
        ].join("&");
        let fullQuery = `${CONFIG.serverUrl}/comments?${query}`;
        return this.http.request(fullQuery)
            .map(this.extractData)
            .catch(this.handleError);
    }

    redactComment(id, text): Rx.Observable<IStatus> {
        let query = [
            `action=redact-comment`,
            `id=${id}`,
            `text=${text}`
        ].join("&");
        let fullQuery = `${CONFIG.serverUrl}/comments?${query}`;
        return this.http.request(fullQuery)
            .map(this.extractData)
            .catch(this.handleError);
    }

    replyComment(id, text, username): Rx.Observable<IStatus> {
        let query = [
            `action=reply-comment`,
            `id=${id}`,
            `text=${text}`,
            `username=${username}`,
        ].join("&");
        let fullQuery = `${CONFIG.serverUrl}/comments?${query}`;
        return this.http.request(fullQuery)
            .map(this.extractData)
            .catch(this.handleError);
    }

    deleteComment(id): Rx.Observable<IStatus> {
        let query = [
            `action=delete-comment`,
            `id=${id}`,
        ].join("&");
        let fullQuery = `${CONFIG.serverUrl}/comments?${query}`;
        return this.http.request(fullQuery)
            .map(this.extractData)
            .catch(this.handleError);
    }

    deleteAllComments(): Rx.Observable<IStatus> {
        let query = [
            `action=delete-comments-all`,
        ].join("&");
        let fullQuery = `${CONFIG.serverUrl}/comments?${query}`;
        return this.http.request(fullQuery)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json() as IComments;
        return body.data || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Rx.Observable.throw(errMsg);
    }
}
