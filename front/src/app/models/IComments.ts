import {ICommentsData} from "./ICommentsData";
export interface IComments {
    result: boolean;
    data: ICommentsData[];
}