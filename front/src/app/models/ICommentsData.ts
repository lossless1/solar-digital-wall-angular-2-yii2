export interface ICommentsData{
    id_comment:number;
    id_parent_comment:number;
    user_name:string;
    text_comment:string;
}
