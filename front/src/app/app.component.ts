import {Component, OnInit} from '@angular/core';
import {AppService} from "./app.service";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'app works!';
    comments = [];

    user: any = '';

    isFormReplyNewComment: boolean = false;
    isFormRedactNewComment: boolean = false;
    isFormNewComment: boolean = true;
    isLoader: boolean = true;

    replyMessage: string;
    redactMessage: string;

    error: boolean = false;
    errorMessage: string;

    _subscription: Subscription;

    redactIdComment: number;
    replyIdComment: number;

    constructor(private appService: AppService) {
        this._subscription = this.appService.missions.subscribe(comments => {
            this.isLoader = false;
            this.comments = comments;
        })
    }

    ngOnInit() {
        this.appService.getCommentsAll();
    }

    deleteComment(index, idComment) {
        this.comments.splice(index, 1);
        this.appService.deleteComment(idComment);
    }


    showReplyComment(index, idComment) {
        this.isFormReplyNewComment = !this.isFormReplyNewComment;
        this.replyIdComment = idComment;
        this.isFormNewComment = this.isFormRedactNewComment ? this.isFormNewComment : !this.isFormNewComment;
        this.isFormRedactNewComment = false;

        this.replyMessage = "@" + this.comments[index].user_name + ",";
    }

    showRedactComment(index, idComment) {
        this.isFormRedactNewComment = !this.isFormRedactNewComment;
        this.redactIdComment = idComment;
        this.isFormNewComment = this.isFormReplyNewComment ? this.isFormNewComment : !this.isFormNewComment;
        this.isFormReplyNewComment = false;

        this.redactMessage = this.comments[index].text_comment;
    }

    addComment(user, comment) {
        if (user.value.length && comment.value.length) {
            this.comments.push({
                'id_parent_comment': '0',
                'user_name': user.value,
                'text_comment': comment.value,
                'created': this.getTimeNow()
            });
            this.appService.addComment(comment.value, user.value);
            user.value = '';
            comment.value = '';
            this.error = false;
        } else {
            this.error = true;
            this.errorMessage = "Input more info!!";
        }
    }

    replyComment(idComment, user) {
        if (this.replyMessage != undefined || user != undefined) {
            this.isFormReplyNewComment = false;
            this.isFormReplyNewComment = false;
            this.isFormNewComment = true;

            this.comments.push({
                'id_parent_comment': idComment,
                'user_name': user,
                'text_comment': this.replyMessage,
                'created': this.getTimeNow()
            });
            this.appService.replyComment(idComment, this.replyMessage, user);
        }
    }

    redactComment(index, idComment) {
        if (this.redactMessage.length) {
            this.isFormRedactNewComment = false;
            this.isFormReplyNewComment = false;
            this.isFormNewComment = true;

            this.comments[index].text_comment = this.redactMessage;
            this.appService.redactComment(idComment, this.redactMessage);
        }
    }

    replyMargin(i, idComment, idParent) {
        if (idParent != 0) {
            return '25px';
        } else {
            return '0';

        }
    }


    getTimeNow(): string {
        let date = new Date();
        return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + " " +
            date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
    }

    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
}
